#include "pch.h"
#include "GameBase.h"

using namespace VoiceMazeDX12UWP;


void GameBase::InitializeGameResources(ID3D12Device *inD3dDevice, LPCWSTR inBaseDir)
{
	m_contentManager = new ContentManager(inD3dDevice, inBaseDir);
}

GameBase::~GameBase()
{
	delete m_contentManager;
	m_contentManager = nullptr;
}