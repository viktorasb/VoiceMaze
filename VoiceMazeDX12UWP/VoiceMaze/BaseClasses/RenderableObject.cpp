#include "pch.h"
#include "RenderableObject.h"

#include "..\..\Common\DeviceResources.h"
#include "Common\DirectXHelper.h"


using namespace VoiceMazeDX12UWP;
using namespace DirectX;

RenderableObject::RenderableObject(Texture *texture)
{
	m_texture = texture;
	SetRotation(XMFLOAT3(0, 0, 0));
	SetPosition(XMFLOAT3(0, 0, 0));
	SetScale(XMFLOAT3(1, 1, 1));
}

RenderableObject::~RenderableObject()
{
	delete m_texture;
}

void RenderableObject::CreateConstantBuffer(ID3D12Device *d3dDevice)
{
	m_perObjectCB.CreateConstantBuffer(d3dDevice);
}

void RenderableObject::CreateVertexBuffer(std::vector<VertexPositionTextureNormal> vertices, ID3D12Device *d3dDevice, const Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList> &commandList)
{
	const UINT vertexBufferSize = vertices.size() * sizeof(VertexPositionTextureNormal);
	CD3DX12_HEAP_PROPERTIES defaultHeapProperties(D3D12_HEAP_TYPE_DEFAULT);
	CD3DX12_HEAP_PROPERTIES uploadHeapProperties(D3D12_HEAP_TYPE_UPLOAD);

	CD3DX12_RESOURCE_DESC vertexBufferDesc = CD3DX12_RESOURCE_DESC::Buffer(vertexBufferSize);
	DX::ThrowIfFailed(d3dDevice->CreateCommittedResource(
		&defaultHeapProperties,
		D3D12_HEAP_FLAG_NONE,
		&vertexBufferDesc,
		D3D12_RESOURCE_STATE_COPY_DEST,
		nullptr,
		IID_PPV_ARGS(&m_vertexBuffer)));

	DX::ThrowIfFailed(d3dDevice->CreateCommittedResource(
		&uploadHeapProperties,
		D3D12_HEAP_FLAG_NONE,
		&vertexBufferDesc,
		D3D12_RESOURCE_STATE_GENERIC_READ,
		nullptr,
		IID_PPV_ARGS(&m_vertexBufferUpload)));

	NAME_D3D12_OBJECT(m_vertexBuffer);

	// Upload the vertex buffer to the GPU.
	{
		D3D12_SUBRESOURCE_DATA vertexData = {};
		vertexData.pData = reinterpret_cast<BYTE*>(vertices.data());
		vertexData.RowPitch = vertexBufferSize;
		vertexData.SlicePitch = vertexData.RowPitch;

		UpdateSubresources(commandList.Get(), m_vertexBuffer.Get(), m_vertexBufferUpload.Get(), 0, 0, 1, &vertexData);

		CD3DX12_RESOURCE_BARRIER vertexBufferResourceBarrier =
			CD3DX12_RESOURCE_BARRIER::Transition(m_vertexBuffer.Get(), D3D12_RESOURCE_STATE_COPY_DEST, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER);
		commandList->ResourceBarrier(1, &vertexBufferResourceBarrier);
	}

	m_vertexBufferView.BufferLocation = m_vertexBuffer->GetGPUVirtualAddress();
	m_vertexBufferView.StrideInBytes = sizeof(VertexPositionTextureNormal);
	m_vertexBufferView.SizeInBytes = vertexBufferSize;
}

void RenderableObject::CreateIndexBuffer(std::vector<unsigned short> indices, ID3D12Device *d3dDevice, const Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList> &commandList)
{
	const UINT indexBufferSize = indices.size() * sizeof(unsigned short);
	m_indexCount = indices.size();
	CD3DX12_HEAP_PROPERTIES uploadHeapProperties(D3D12_HEAP_TYPE_UPLOAD);
	CD3DX12_HEAP_PROPERTIES defaultHeapProperties(D3D12_HEAP_TYPE_DEFAULT);

	CD3DX12_RESOURCE_DESC indexBufferDesc = CD3DX12_RESOURCE_DESC::Buffer(indexBufferSize);
	DX::ThrowIfFailed(d3dDevice->CreateCommittedResource(
		&defaultHeapProperties,
		D3D12_HEAP_FLAG_NONE,
		&indexBufferDesc,
		D3D12_RESOURCE_STATE_COPY_DEST,
		nullptr,
		IID_PPV_ARGS(&m_indexBuffer)));

	DX::ThrowIfFailed(d3dDevice->CreateCommittedResource(
		&uploadHeapProperties,
		D3D12_HEAP_FLAG_NONE,
		&indexBufferDesc,
		D3D12_RESOURCE_STATE_GENERIC_READ,
		nullptr,
		IID_PPV_ARGS(&m_indexBufferUpload)));

	NAME_D3D12_OBJECT(m_indexBuffer);

	// Upload the index buffer to the GPU.
	{
		D3D12_SUBRESOURCE_DATA indexData = {};
		indexData.pData = reinterpret_cast<BYTE*>(indices.data());
		indexData.RowPitch = indexBufferSize;
		indexData.SlicePitch = indexData.RowPitch;

		UpdateSubresources(commandList.Get(), m_indexBuffer.Get(), m_indexBufferUpload.Get(), 0, 0, 1, &indexData);

		CD3DX12_RESOURCE_BARRIER indexBufferResourceBarrier =
			CD3DX12_RESOURCE_BARRIER::Transition(m_indexBuffer.Get(), D3D12_RESOURCE_STATE_COPY_DEST, D3D12_RESOURCE_STATE_INDEX_BUFFER);
		commandList->ResourceBarrier(1, &indexBufferResourceBarrier);
	}

	m_indexBufferView.BufferLocation = m_indexBuffer->GetGPUVirtualAddress();
	m_indexBufferView.SizeInBytes = indexBufferSize;
	m_indexBufferView.Format = DXGI_FORMAT_R16_UINT;
}

void RenderableObject::Render(const Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList> &commandList, UINT8 frameIndex, Camera *camera)
{
	{
		// update camera
		auto viewProjectionMatrix = camera->CalculateViewMat() * camera->CalculateProjectionMat();

		auto rotations = GetRotation();
		auto position = GetPosition();
		auto scale = GetScale();

		auto worldTransform = XMMatrixRotationX(rotations.x) * XMMatrixRotationY(rotations.y) * XMMatrixRotationZ(rotations.z) * XMMatrixScaling(scale.x, scale.y, scale.z) * XMMatrixTranslation(position.x, position.y, position.z);

		auto transformationsMatrix = worldTransform * viewProjectionMatrix;

		//float3 v = normalize(mul(normalize(ViewVector), World));
		
		//XMVector4Normalize()
		auto viewVector = XMLoadFloat4(&camera->GetViewVector());
		auto premultipliedViewVector = XMVector4Normalize(XMVector4Transform(XMVector4Normalize(viewVector), XMMatrixTranspose(worldTransform)));

		auto perObjectData = PerObjectConstantBuffer();
		XMStoreFloat4x4(&perObjectData.Transformations, XMMatrixTranspose(transformationsMatrix));
		XMStoreFloat4x4(&perObjectData.WorldInverseTranspose, XMMatrixInverse(nullptr, worldTransform));
		XMStoreFloat4(&perObjectData.PremultipliedViewVector, premultipliedViewVector);

		m_perObjectCB.UpdateBuffer(perObjectData, frameIndex, 0, commandList);
	}

	{
		// Update texture
		ID3D12DescriptorHeap* ppHeaps2[] = { m_texture->GetTextureDescriptor() };
		commandList->SetDescriptorHeaps(_countof(ppHeaps2), ppHeaps2);

		commandList->SetGraphicsRootDescriptorTable(2, m_texture->GetTextureDescriptor()->GetGPUDescriptorHandleForHeapStart());
	}

	commandList->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	commandList->IASetVertexBuffers(0, 1, &m_vertexBufferView);
	commandList->IASetIndexBuffer(&m_indexBufferView);
	commandList->DrawIndexedInstanced(m_indexCount, 1, 0, 0, 0);
}