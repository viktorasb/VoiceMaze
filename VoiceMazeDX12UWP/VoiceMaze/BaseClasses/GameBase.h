#pragma once

#include "..\CommonElements\ContentManager.h"

namespace VoiceMazeDX12UWP
{
	class GameBase
	{
		// Methods
	public:
		GameBase() {}
		~GameBase();

		void InitializeGameResources(ID3D12Device *inD3dDevice, LPCWSTR inBaseDir);

	protected:
		ID3D12GraphicsCommandList* GetCommandList()
		{
			return m_commandList.Get();
		}

		ContentManager* GetContentManager()
		{
			return m_contentManager;
		}


		// Members
	private:
		Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList>	m_commandList;
		ContentManager*										m_contentManager;
	};
}