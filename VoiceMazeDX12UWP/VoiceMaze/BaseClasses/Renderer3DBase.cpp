#include "pch.h"
#include "Renderer3DBase.h"
#include "..\..\Common\DirectXHelper.h"
#include "..\CommonElements\Camera.h"

using namespace DirectX;
using namespace Microsoft::WRL;
using namespace VoiceMazeDX12UWP;


Renderer3DBase::Renderer3DBase(const std::shared_ptr<DX::DeviceResources>& deviceResources)
	:m_deviceResources(deviceResources)
{
	m_lightSource = new LightSource();
}

Renderer3DBase::~Renderer3DBase()
{
	delete m_camera;
	m_camera = nullptr;

	delete m_lightSource;
	m_lightSource = nullptr;
}

void Renderer3DBase::Initialize(std::wstring vertexShaderName, std::wstring pixelShaderName, ContentManager *inContentManager)
{
	m_contentManager = inContentManager;

	LoadState();

	CreateDeviceDependentResources(vertexShaderName, pixelShaderName);
	CreateWindowSizeDependentResources();
}

void Renderer3DBase::CreateDeviceDependentResources(std::wstring vertexShaderName, std::wstring pixelShaderName)
{
	auto d3dDevice = m_deviceResources->GetD3DDevice();

	// Create a root signature with a single constant buffer slot.
	{
		D3D12_ROOT_DESCRIPTOR rootCBVDescriptor;
		rootCBVDescriptor.RegisterSpace = 0;
		rootCBVDescriptor.ShaderRegister = 0;

		CD3DX12_ROOT_PARAMETER rootParameters[3];
		{
			CD3DX12_DESCRIPTOR_RANGE range;

			range.Init(D3D12_DESCRIPTOR_RANGE_TYPE_CBV, 1, 0);
			rootParameters[0].InitAsDescriptorTable(1, &range, D3D12_SHADER_VISIBILITY_VERTEX);
		}

		{
			CD3DX12_DESCRIPTOR_RANGE range;

			range.Init(D3D12_DESCRIPTOR_RANGE_TYPE_CBV, 1, 1);
			//in some cases like normal maps the constant buffer data might be needed in pixel shader calculations too. So: visibility-ALL
			rootParameters[1].InitAsDescriptorTable(1, &range, D3D12_SHADER_VISIBILITY_ALL);
		}

		{
			CD3DX12_DESCRIPTOR_RANGE range;
			range.Init(D3D12_DESCRIPTOR_RANGE_TYPE_SRV, 1, 0);

			D3D12_ROOT_DESCRIPTOR_TABLE descriptorTable;
			descriptorTable.NumDescriptorRanges = 1; // we only have one range
			descriptorTable.pDescriptorRanges = &range; // the pointer to the beginning of our ranges array

			rootParameters[2].ParameterType = D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE; // this is a descriptor table
			rootParameters[2].DescriptorTable = descriptorTable; // this is our descriptor table for this root parameter
			rootParameters[2].ShaderVisibility = D3D12_SHADER_VISIBILITY_PIXEL; // our pixel shader will be the only shader accessing this parameter for now
		}

		// create a static sampler
		D3D12_STATIC_SAMPLER_DESC sampler = {};
		sampler.Filter = D3D12_FILTER_MIN_MAG_MIP_POINT;
		sampler.AddressU = D3D12_TEXTURE_ADDRESS_MODE_BORDER;
		sampler.AddressV = D3D12_TEXTURE_ADDRESS_MODE_BORDER;
		sampler.AddressW = D3D12_TEXTURE_ADDRESS_MODE_BORDER;
		sampler.MipLODBias = 0;
		sampler.MaxAnisotropy = 0;
		sampler.ComparisonFunc = D3D12_COMPARISON_FUNC_NEVER;
		sampler.BorderColor = D3D12_STATIC_BORDER_COLOR_TRANSPARENT_BLACK;
		sampler.MinLOD = 0.0f;
		sampler.MaxLOD = D3D12_FLOAT32_MAX;
		sampler.ShaderRegister = 0;
		sampler.RegisterSpace = 0;
		sampler.ShaderVisibility = D3D12_SHADER_VISIBILITY_PIXEL;

		D3D12_ROOT_SIGNATURE_FLAGS rootSignatureFlags =
			D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT | // Only the input assembler stage needs access to the constant buffer.
			D3D12_ROOT_SIGNATURE_FLAG_DENY_HULL_SHADER_ROOT_ACCESS |
			D3D12_ROOT_SIGNATURE_FLAG_DENY_DOMAIN_SHADER_ROOT_ACCESS |
			D3D12_ROOT_SIGNATURE_FLAG_DENY_GEOMETRY_SHADER_ROOT_ACCESS;

		CD3DX12_ROOT_SIGNATURE_DESC descRootSignature;
		descRootSignature.Init(_countof(rootParameters), rootParameters, 1, &sampler, rootSignatureFlags);

		ComPtr<ID3DBlob> pSignature;
		ComPtr<ID3DBlob> pError;
		DX::ThrowIfFailed(D3D12SerializeRootSignature(&descRootSignature, D3D_ROOT_SIGNATURE_VERSION_1, pSignature.GetAddressOf(), pError.GetAddressOf()));
		DX::ThrowIfFailed(d3dDevice->CreateRootSignature(0, pSignature->GetBufferPointer(), pSignature->GetBufferSize(), IID_PPV_ARGS(&m_rootSignature)));
		NAME_D3D12_OBJECT(m_rootSignature);
	}

	// Load shaders asynchronously.
	auto createVSTask = DX::ReadDataAsync(vertexShaderName).then([this](std::vector<byte>& fileData) {
		m_vertexShader = fileData;
	});

	auto createPSTask = DX::ReadDataAsync(pixelShaderName).then([this](std::vector<byte>& fileData) {
		m_pixelShader = fileData;
	});

	// Create the pipeline state once the shaders are loaded.
	auto createPipelineStateTask = (createPSTask && createVSTask).then([this, d3dDevice]() {
		// fill out an input layout description structure
		D3D12_INPUT_LAYOUT_DESC inputLayoutDesc = GetInputLayoutDescriptor();


		{
			D3D12_GRAPHICS_PIPELINE_STATE_DESC state = {};
			state.InputLayout = inputLayoutDesc;
			state.pRootSignature = m_rootSignature.Get();
			state.VS = CD3DX12_SHADER_BYTECODE(&m_vertexShader[0], m_vertexShader.size());
			state.PS = CD3DX12_SHADER_BYTECODE(&m_pixelShader[0], m_pixelShader.size());
			state.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
			state.RTVFormats[0] = m_deviceResources->GetBackBufferFormat();
			state.SampleDesc.Count = 1;
			state.SampleMask = UINT_MAX;
			state.RasterizerState = CD3DX12_RASTERIZER_DESC(D3D12_DEFAULT);
			state.BlendState = CD3DX12_BLEND_DESC(D3D12_DEFAULT);
			state.NumRenderTargets = 1;
			state.DepthStencilState = CD3DX12_DEPTH_STENCIL_DESC(D3D12_DEFAULT);
			state.DSVFormat = m_deviceResources->GetDepthBufferFormat();

			DX::ThrowIfFailed(m_deviceResources->GetD3DDevice()->CreateGraphicsPipelineState(&state, IID_PPV_ARGS(&m_pipelineState)));
		}

		// Shader data can be deleted once the pipeline state is created.
		m_vertexShader.clear();
		m_pixelShader.clear();
	});

	auto createCommandListTask = createPipelineStateTask.then([this, d3dDevice]() {
		// Create a command list.
		DX::ThrowIfFailed(d3dDevice->CreateCommandList(0, D3D12_COMMAND_LIST_TYPE_DIRECT, m_deviceResources->GetCommandAllocator(), m_pipelineState.Get(), IID_PPV_ARGS(&m_commandList)));
		NAME_D3D12_OBJECT(m_commandList);
	});

	auto internalInitializationTask = createCommandListTask.then([this, d3dDevice]() {
		InternalInitializationSteps(d3dDevice);
	});

	auto createAssetsTask = (internalInitializationTask).then([this, d3dDevice]() {
		// Close the command list and execute it to begin the vertex/index buffer copy into the GPU's default heap.
		DX::ThrowIfFailed(m_commandList->Close());
		ID3D12CommandList* ppCommandLists[] = { m_commandList.Get() };
		m_deviceResources->GetCommandQueue()->ExecuteCommandLists(_countof(ppCommandLists), ppCommandLists);

		// Wait for the command list to finish executing; the vertex/index buffers need to be uploaded to the GPU before the upload resources go out of scope.
		m_deviceResources->WaitForGpu();
	});

	createAssetsTask.then([this]() {
		m_loadingComplete = true;
	});

}

void Renderer3DBase::CreateWindowSizeDependentResources()
{
	if (m_camera == nullptr)
		 m_camera = new Camera(m_deviceResources);

	m_camera->Initialize(m_deviceResources->GetOutputSize(), m_deviceResources->GetScreenViewport());
}

void Renderer3DBase::SaveState()
{
	InternalStateSavingSteps();
}

void Renderer3DBase::LoadState()
{
	InternalStateLoadingSteps();
}


void VoiceMazeDX12UWP::Renderer3DBase::Update(DX::StepTimer const& timer)
{
	if (m_loadingComplete)
	{
		InternalUpdateSteps(timer.GetElapsedSeconds());
	}
}

bool VoiceMazeDX12UWP::Renderer3DBase::Render()
{
	if (!m_loadingComplete)
	{
		return false;
	}


	DX::ThrowIfFailed(m_deviceResources->GetCommandAllocator()->Reset());

	// The command list can be reset anytime after ExecuteCommandList() is called.
	DX::ThrowIfFailed(m_commandList->Reset(m_deviceResources->GetCommandAllocator(), m_pipelineState.Get()));

	PIXBeginEvent(m_commandList.Get(), 0, L"Draw the scene");
	{
		// Set the graphics root signature and descriptor heaps to be used by this frame.
		m_commandList->SetGraphicsRootSignature(m_rootSignature.Get());

		//normal drawing
		m_commandList->SetPipelineState(m_pipelineState.Get());

		// Set the viewport and scissor rectangle.
		D3D12_VIEWPORT viewport = m_deviceResources->GetScreenViewport();
		m_commandList->RSSetViewports(1, &viewport);
		m_commandList->RSSetScissorRects(1, &m_camera->GetScissorRectangle());

		// Indicate this resource will be in use as a render target.
		CD3DX12_RESOURCE_BARRIER renderTargetResourceBarrier =
			CD3DX12_RESOURCE_BARRIER::Transition(GetRenderTarget(), D3D12_RESOURCE_STATE_PRESENT, D3D12_RESOURCE_STATE_RENDER_TARGET);
		m_commandList->ResourceBarrier(1, &renderTargetResourceBarrier);

		InternalRenderSteps();


		// Indicate that the render target will now be used to present when the command list is done executing.
		CD3DX12_RESOURCE_BARRIER presentResourceBarrier =
			CD3DX12_RESOURCE_BARRIER::Transition(GetRenderTarget(), D3D12_RESOURCE_STATE_RENDER_TARGET, D3D12_RESOURCE_STATE_PRESENT);
		m_commandList->ResourceBarrier(1, &presentResourceBarrier);
	}
	PIXEndEvent(m_commandList.Get());

	DX::ThrowIfFailed(m_commandList->Close());

	// Execute the command list.
	ID3D12CommandList* ppCommandLists[] = { m_commandList.Get() };
	m_deviceResources->GetCommandQueue()->ExecuteCommandLists(_countof(ppCommandLists), ppCommandLists);


	return true;
}
