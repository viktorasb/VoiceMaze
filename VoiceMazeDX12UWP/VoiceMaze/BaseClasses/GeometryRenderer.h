#pragma once

#include <wtypes.h>

namespace VoiceMazeDX12UWP
{
	class GeometryRenderer
	{
		// Methods
	public:
		virtual void RenderGeometry(UINT frameIndex) = 0;


		// Members
	};

}