#pragma once

#include "..\..\Common\StepTimer.h"
#include "..\..\Common\DeviceResources.h"
#include "..\CommonElements\ContentManager.h"
#include "..\Content\ShaderStructures.h"
#include "..\CommonElements\Camera.h"
#include "..\CommonElements\LightSource.h"
#include "GeometryRenderer.h"


namespace VoiceMazeDX12UWP
{
	class Renderer3DBase : public GeometryRenderer
	{
		// Methods
	public:
		Renderer3DBase(const std::shared_ptr<DX::DeviceResources>& deviceResource);
		~Renderer3DBase();

		void CreateDeviceDependentResources(std::wstring vertexShaderName, std::wstring pixelShaderName);
		void CreateWindowSizeDependentResources();

		void SaveState();
		void LoadState();

		void Update(DX::StepTimer const& timer);
		bool Render();

	protected:
		void Initialize(std::wstring pixelShaderName, std::wstring vertexShaderName, ContentManager *inContentManager);

		virtual Concurrency::task<void> InternalInitializationSteps(ID3D12Device *d3dDevice) = 0;

		virtual GeometryRenderer* GetGeometryRenderer() = 0;

		virtual void InternalStateSavingSteps() = 0;
		virtual void InternalStateLoadingSteps() = 0;

		virtual void InternalUpdateSteps(double dTime) = 0;
		virtual bool InternalRenderSteps() = 0;

		virtual ID3D12Resource* GetRenderTarget() = 0;
		virtual D3D12_INPUT_LAYOUT_DESC GetInputLayoutDescriptor() = 0;

	private:


		// Members
	protected:
		static const UINT c_alignedConstantBufferSize = (sizeof(PerFrameConstantBuffer) + 255) & ~255;

		Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList>	m_commandList;

		Microsoft::WRL::ComPtr<ID3D12RootSignature>		m_rootSignature;
		Microsoft::WRL::ComPtr<ID3D12PipelineState>		m_pipelineState;
		std::shared_ptr<DX::DeviceResources>			m_deviceResources;

		std::vector<byte>								m_vertexShader;
		std::vector<byte>								m_pixelShader;

		LightSource*		m_lightSource;
		Camera*				m_camera;
		ContentManager*		m_contentManager;
		bool				m_loadingComplete;

	private:

	};

}