#pragma once

#include "..\DataTransferObjects\BufferDescriptor.h"
#include "..\CommonElements\Camera.h"
#include "Content\ShaderStructures.h"
#include "..\CommonElements\Texture.h"

namespace VoiceMazeDX12UWP
{
	private class RenderableObject
	{
		// Methods
	public:
		RenderableObject(Texture *texture);
		~RenderableObject();

		void RenderableObject::CreateConstantBuffer(ID3D12Device *d3dDevice);
		void CreateVertexBuffer(std::vector<VertexPositionTextureNormal> vertices, ID3D12Device *d3dDevice, const Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList> &commandList);
		void CreateIndexBuffer(std::vector<unsigned short> indices, ID3D12Device *d3dDevice, const Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList> &commandList);

		virtual void Render(const Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList> &commandList, UINT8 frameIndex, Camera* camera);

		void SetPosition(DirectX::XMFLOAT3 inPosition)	{ m_position = inPosition; }
		void SetRotation(DirectX::XMFLOAT3 inRotation)	{ m_rotation = inRotation; }
		void SetScale(DirectX::XMFLOAT3 inScale)		{ m_scale = inScale; }

		DirectX::XMFLOAT3 GetPosition()					{ return m_position; }
		DirectX::XMFLOAT3 GetRotation()					{ return m_rotation; }
		DirectX::XMFLOAT3 GetScale()					{ return m_scale; }

		bool IsShadowCaster()							{ return m_castShadow; }
		void IsShadowCaster(bool castsShadow)			{ m_castShadow = castsShadow; }


		// Members
	protected:
		DirectX::XMFLOAT3	m_position;
		DirectX::XMFLOAT3	m_rotation;
		DirectX::XMFLOAT3	m_scale;

		Texture*			m_texture;

	private:
		BufferDescriptor<PerObjectConstantBuffer>			m_perObjectCB;

		// The upload resources must not be released until after the GPU has finished using it.
		Microsoft::WRL::ComPtr<ID3D12Resource>				m_vertexBufferUpload;
		Microsoft::WRL::ComPtr<ID3D12Resource>				m_indexBufferUpload;

		Microsoft::WRL::ComPtr<ID3D12Resource>				m_vertexBuffer;
		Microsoft::WRL::ComPtr<ID3D12Resource>				m_indexBuffer;
		D3D12_VERTEX_BUFFER_VIEW							m_vertexBufferView;
		D3D12_INDEX_BUFFER_VIEW								m_indexBufferView;
		int													m_indexCount;
		bool												m_castShadow;
	};

}
