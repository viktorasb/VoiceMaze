#include "pch.h"
#include "MazeInput.h"

using namespace VoiceMazeDX12UWP;

MazeInput::MazeInput()
{
	m_lastInput = EInput::EInput_NONE;
}

MazeInput::~MazeInput()
{
	m_voiceInput.Stop();
}

void MazeInput::UpdateInput()
{
	if (!m_voiceInput.IsActive())
		m_voiceInput.Start();


	if (!ProcessVoiceInput())
		ProcessKeyboardInput();

}

void MazeInput::ProcessKeyboardInput()
{
	if (m_gameInput.IsKeyPressed(Windows::System::VirtualKey::Up))
		m_lastInput = EInput::EInput_UP;
	else if (m_gameInput.IsKeyPressed(Windows::System::VirtualKey::Down))
		m_lastInput = EInput::EInput_DOWN;
	else if (m_gameInput.IsKeyPressed(Windows::System::VirtualKey::Left))
		m_lastInput = EInput::EInput_LEFT;
	else if (m_gameInput.IsKeyPressed(Windows::System::VirtualKey::Right))
		m_lastInput = EInput::EInput_RIGHT;
	else m_lastInput = EInput::EInput_NONE;
}

bool MazeInput::ProcessVoiceInput()
{
	bool inputReceived = false;

	auto lastVoiceCommand = m_voiceInput.GetLastReading(true);
	if (lastVoiceCommand->Equals("up"))
	{
		m_lastInput = EInput::EInput_UP;
		inputReceived = true;
	}
	else if (lastVoiceCommand->Equals("down"))
	{
		m_lastInput = EInput::EInput_DOWN;
		inputReceived = true;
	}
	else if (lastVoiceCommand->Equals("left"))
	{
		m_lastInput = EInput::EInput_LEFT;
		inputReceived = true;
	}
	else if (lastVoiceCommand->Equals("right"))
	{
		m_lastInput = EInput::EInput_RIGHT;
		inputReceived = true;
	}
	else m_lastInput = EInput::EInput_NONE;

	return inputReceived;
}