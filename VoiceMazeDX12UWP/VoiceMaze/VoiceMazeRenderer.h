#pragma once

#include "BaseClasses\Renderer3DBase.h"
#include "BaseClasses\GeometryRenderer.h"
#include "..\Common\DeviceResources.h"
#include "..\Content\ShaderStructures.h"

#include "CommonElements\LightSource.h"
#include "MazeInput.h"
#include "PlayerAvatar.h"
#include "Maze.h"

namespace VoiceMazeDX12UWP
{
	class VoiceMazeRenderer : public Renderer3DBase, public GeometryRenderer
	{
		// Constants
		const float KCameraDistance = 3;
		const DirectX::XMFLOAT3 KCameraAwayDirection = DirectX::XMFLOAT3(0.0f, 0.75f, -0.30f);


		// Methods
	public:
		VoiceMazeRenderer(const std::shared_ptr<DX::DeviceResources>& deviceResources, ContentManager* inContentManager);
		~VoiceMazeRenderer();

	protected:
		Concurrency::task<void> InternalInitializationSteps(ID3D12Device *d3dDevice);

		void InternalStateSavingSteps();
		void InternalStateLoadingSteps();

		void UpdateCamera();
		void InternalUpdateSteps(double dTime);

		GeometryRenderer* GetGeometryRenderer() { return this; }

		bool InternalRenderSteps();
		void RenderGeometry(UINT frameIndex);

		// Inherited via Renderer3DBase
		virtual ID3D12Resource * GetRenderTarget();
		virtual D3D12_INPUT_LAYOUT_DESC GetInputLayoutDescriptor();
		
		// Members
	private:
		MazeInput											m_mazeInput;
		BufferDescriptor<PerFrameConstantBuffer>			m_perFrameCB;
		Maze*												m_maze;
		PlayerAvatar*										m_playerAvatar;
	};
}

