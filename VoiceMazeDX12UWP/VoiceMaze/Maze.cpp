#include "pch.h"
#include "Maze.h"

#include "MazeElement.h"



using namespace VoiceMazeDX12UWP;

Maze::Maze()
{
}

Maze::~Maze()
{
	for (unsigned int i = 0; i < m_mazeElementInstances.size(); i++)
		for (unsigned int j = 0; j < m_mazeElementInstances[i].size(); j++)
			delete m_mazeElementInstances[i][j];

	m_mazeElementInstances.clear();
	m_mazeElementTypes.clear();
}

void Maze::Initialize(ID3D12Device *d3dDevice, const Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList> &commandList, ContentManager *contentManager)
{
	const int KMazeData[] =
	{
		0,0,0,0,0,-1,
		0,1,1,0,1,1,
		1,0,1,0,0,0,
		0,0,0,1,1,0,
		0,1,0,0,1,0,
		-2,1,0,0,0,0
	};
	int numCols = 6;
	int numRows = 6;

	//create the maze

	bool enclosed = CheckIfEnclosed(KMazeData, numRows, numCols);
	if (!enclosed)
	{
		m_mazeElementTypes.resize(numRows + 2);
		for (int i = 0; i < numRows + 2; i++)
			m_mazeElementTypes[i].resize(numCols + 2);

		for (unsigned int i = 0; i < m_mazeElementTypes.size(); i++)
			for (unsigned int j = 0; j < m_mazeElementTypes[i].size(); j++)
			{
				if (i == 0 || i == m_mazeElementTypes.size() - 1 || j == 0 || j == m_mazeElementTypes[i].size() - 1)
				{
					m_mazeElementTypes[i][j] = 1;//set to wall
				}
				else m_mazeElementTypes[i][j] = KMazeData[(numRows-i )*numCols + j - 1];
			}
	}
	else
	{
		m_mazeElementTypes.resize(numRows);
		for (int i = 0; i < numRows; i++)
			m_mazeElementTypes[i].resize(numCols);

		for (unsigned int i = 0; i < m_mazeElementTypes.size(); i++)
			for (unsigned int j = 0; j < m_mazeElementTypes[i].size(); j++)
				m_mazeElementTypes[i][j] = KMazeData[(numRows-i-1)*numCols + (j)];
	}


	//create graphical elements
	m_mazeElementInstances.resize(m_mazeElementTypes.size());
	for (unsigned int i = 0; i < m_mazeElementTypes.size(); i++)
		m_mazeElementInstances[i].resize(m_mazeElementTypes[i].size());

	auto wallTexture = contentManager->Load<Texture>(L"Textures\\walltex.png", commandList.Get());
	auto floorTexture = contentManager->Load<Texture>(L"Textures\\floortex.png", commandList.Get());
	auto floorTextureS = contentManager->Load<Texture>(L"Textures\\floortexS.png", commandList.Get());
	auto floorTextureF = contentManager->Load<Texture>(L"Textures\\floortexF.png", commandList.Get());
	Texture* textures[] = { floorTextureF, floorTextureS, floorTexture, wallTexture};

	for (unsigned int i = 0; i < m_mazeElementInstances.size(); i++)
		for (unsigned int j = 0; j < m_mazeElementInstances[i].size(); j++)
		{
			auto type = m_mazeElementTypes[i][j];
			auto element = new MazeElement(d3dDevice, commandList, textures[type +2]);
			if (type == 1)
				element->SetPosition(DirectX::XMFLOAT3(j, 0.5f, i));
			else element->SetPosition(DirectX::XMFLOAT3(j, 0, i));
			m_mazeElementInstances[i][j] = element;

			if (type == -1)//start position
				m_startPosition = DirectX::XMFLOAT2(j, i);
		}

}

bool Maze::IsExitFound(PlayerAvatar *playerAvatar)
{
	if (playerAvatar->IsMoving())
		return false;
	
	auto avatarPos = playerAvatar->GetCurrentPosition();

	return m_mazeElementTypes[(int)avatarPos.x][(int)avatarPos.z] == -2;
}

void Maze::ProcessInput(EInput input, PlayerAvatar *playerAvatar)
{
	if (playerAvatar->IsMoving())
		return;

	auto currPos = playerAvatar->GetCurrentPositionIndex();
	auto avatarY = playerAvatar->GetCurrentPosition().y;

	switch (input)
	{
	case EInput::EInput_DOWN://up down might seem inverted, this happens because of the way it's stored in the array
		if (currPos.y> 1)
			if (m_mazeElementTypes[currPos.y - 1][currPos.x] != 1)
				playerAvatar->MoveAvatar(DirectX::XMFLOAT3(currPos.x, avatarY, currPos.y - 1));
		break;

	case EInput::EInput_UP://up down might seem inverted, this happens because of the way it's stored in the array
		if (currPos.y < (int)m_mazeElementTypes.size() - 2)
			if (m_mazeElementTypes[currPos.y + 1][currPos.x] != 1)
				playerAvatar->MoveAvatar(DirectX::XMFLOAT3(currPos.x, avatarY, currPos.y + 1));
		break;

	case EInput::EInput_LEFT:
		if (currPos.x > 1)
			if (m_mazeElementTypes[currPos.y][currPos.x - 1] != 1)
				playerAvatar->MoveAvatar(DirectX::XMFLOAT3(currPos.x - 1, avatarY, currPos.y));
		break;

	case EInput::EInput_RIGHT:
		if (currPos.x < (int)m_mazeElementTypes[0].size() - 2)
			if (m_mazeElementTypes[currPos.y][currPos.x + 1] != 1)
				playerAvatar->MoveAvatar(DirectX::XMFLOAT3(currPos.x + 1, avatarY, currPos.y));
		break;
	}


}


void Maze::Render(const Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList> &commandList, UINT8 frameIndex, Camera *camera)
{
	for (unsigned int i = 0; i < m_mazeElementInstances.size(); i++)
		for (unsigned int j = 0; j < m_mazeElementInstances[i].size(); j++)
		{
			m_mazeElementInstances[i][j]->Render(commandList, frameIndex, camera);
		}
}

bool Maze::CheckIfEnclosed(const int *mazeData, int numRows, int numCols)
{
	bool noGaps = true;
	for (int i = 0; i < numRows && noGaps; i++)
		for (int j = 0; j < numCols && noGaps; j++)
		{
			if (i == 0 || i == numRows - 1 || j == 0 || j == numCols - 1)
				if (mazeData[i*numCols + j] != 1)
					noGaps = false;
		}

	return noGaps;
}