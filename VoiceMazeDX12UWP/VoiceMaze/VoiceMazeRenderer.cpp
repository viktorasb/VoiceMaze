#include "pch.h"
#include "VoiceMazeRenderer.h"
#include "Helpers\XMFloatMath.h"

#include "DataTransferObjects\BufferDescriptor.h"


using namespace VoiceMazeDX12UWP;
using namespace DirectX;

VoiceMazeRenderer::VoiceMazeRenderer(const std::shared_ptr<DX::DeviceResources>& deviceResources, ContentManager* inContentManager) : Renderer3DBase(deviceResources)
{
	Renderer3DBase::Initialize( L"SampleVertexShader.cso", L"SamplePixelShader.cso", inContentManager);

	m_lightSource->SetLightDirection(XMFLOAT4(0.2f, 1, 1, 0));
	//m_lightSource.SetColor(XMFLOAT4(1, 0.7, 0.296, 1));
	m_lightSource->SetColor(XMFLOAT4(1, 1, 1, 1));
}

VoiceMazeRenderer::~VoiceMazeRenderer()
{
	delete m_maze;
	m_maze = nullptr;

	delete m_playerAvatar;
	m_playerAvatar = nullptr;
}

Concurrency::task<void> VoiceMazeRenderer::InternalInitializationSteps(ID3D12Device *d3dDevice)
{
	m_perFrameCB.CreateConstantBuffer(d3dDevice);

	m_maze = new Maze();
	m_maze->Initialize(d3dDevice, m_commandList, m_contentManager);
	
	auto avatarMesh = m_contentManager->Load<MeshObject>(L"Models\\crown", m_commandList.Get());
	m_playerAvatar = new PlayerAvatar(avatarMesh);
	m_playerAvatar->MoveAvatar(XMFLOAT3(m_maze->GetStartPosition().x, m_playerAvatar->GetCurrentPosition().y, m_maze->GetStartPosition().y), true);

	return Concurrency::create_task([](){});
}

void VoiceMazeRenderer::InternalStateSavingSteps()
{}

void VoiceMazeRenderer::InternalStateLoadingSteps() 
{}

void VoiceMazeRenderer::InternalUpdateSteps(double dTime)
{
	m_mazeInput.UpdateInput();
	m_maze->ProcessInput(m_mazeInput.GetLastInput(), m_playerAvatar);

	m_playerAvatar->Update(dTime);

	UpdateCamera();

	
	if (!m_playerAvatar->IsMoving() && m_maze->IsExitFound(m_playerAvatar))
		m_playerAvatar->PlayEndAnimation(m_maze->GetStartPosition());
}

void VoiceMazeRenderer::UpdateCamera()
{
	auto avatarPos = m_playerAvatar->GetCurrentPosition();
	m_camera->SetLookAtPoint(XMFLOAT4(avatarPos.x, avatarPos.y, avatarPos.z, 0));

	auto cameraEye = avatarPos + KCameraAwayDirection * KCameraDistance;
	m_camera->SetEyePosition(XMFLOAT4(cameraEye.x, cameraEye.y, cameraEye.z, 0));
}

bool VoiceMazeRenderer::InternalRenderSteps()
{
	auto frameIndex = m_deviceResources->GetCurrentFrameIndex();

	// Record drawing commands.
	D3D12_CPU_DESCRIPTOR_HANDLE renderTargetView = m_deviceResources->GetRenderTargetView();
	D3D12_CPU_DESCRIPTOR_HANDLE depthStencilView = m_deviceResources->GetDepthStencilView();
	m_commandList->ClearRenderTargetView(renderTargetView, DirectX::Colors::CornflowerBlue, 0, nullptr);
	m_commandList->ClearDepthStencilView(depthStencilView, D3D12_CLEAR_FLAG_DEPTH, 1.0f, 0, 0, nullptr);

	m_commandList->OMSetRenderTargets(1, &renderTargetView, false, &depthStencilView);
		
	{
		RenderGeometry(frameIndex);
	}

	return true;
}


void VoiceMazeRenderer::RenderGeometry(UINT frameIndex)
{
	// Update per-frame constant buffer
	{
		PerFrameConstantBuffer perFrameData = PerFrameConstantBuffer();
		perFrameData.DiffuseLightColor = m_lightSource->GetColor();
		perFrameData.DiffuseLightDirection = m_lightSource->GetDirection();

		m_perFrameCB.UpdateBuffer(perFrameData, frameIndex, 1, m_commandList);
	}

	m_maze->Render(m_commandList, frameIndex, m_camera);
	m_playerAvatar->Render(m_commandList, frameIndex, m_camera);
}

ID3D12Resource * VoiceMazeDX12UWP::VoiceMazeRenderer::GetRenderTarget()
{
	return m_deviceResources->GetRenderTarget();
}

D3D12_INPUT_LAYOUT_DESC VoiceMazeDX12UWP::VoiceMazeRenderer::GetInputLayoutDescriptor()
{
	static D3D12_INPUT_ELEMENT_DESC layout[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 }
	};

	D3D12_INPUT_LAYOUT_DESC inputLayoutDesc = {};

	// we can get the number of elements in an array by "sizeof(array) / sizeof(arrayElementType)"
	inputLayoutDesc.NumElements = sizeof(layout) / sizeof(D3D12_INPUT_ELEMENT_DESC);
	inputLayoutDesc.pInputElementDescs = layout;

	return inputLayoutDesc;
}
