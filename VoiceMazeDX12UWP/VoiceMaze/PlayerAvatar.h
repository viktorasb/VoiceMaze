#pragma once

#include "CommonElements\MeshObject.h"

namespace VoiceMazeDX12UWP
{
	private class PlayerAvatar
	{
		//Constants
		const float KMovementSpeed = 1.0f;
		const float KIdleRotationSpeed = DirectX::XM_PIDIV4 / 2;
		const float KInitialLift = 0.5;
		const DirectX::XMFLOAT3 KInitialScale = DirectX::XMFLOAT3(0.7f, 0.7f, 0.7f);

		// win animation constants
		const float KWinRotationSpeed = DirectX::XM_PIDIV2;
		float KWinLiftAmount = 3;
		const float KWinStepTime = 2;//2seconds


		//Methods
	public:
		PlayerAvatar(MeshObject* inMeshObject);
		~PlayerAvatar();

		void Update(float dTime);
		void Render(const Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList> &commandList, UINT8 frameIndex, Camera* camera);

		void MoveAvatar(DirectX::XMFLOAT3 newPos, bool forceImmediate = false);
		
		void PlayEndAnimation(DirectX::XMFLOAT2 startPosition);

		DirectX::XMFLOAT2	GetCurrentPositionIndex();
		DirectX::XMFLOAT3	GetCurrentPosition()	{ return m_meshObject->GetPosition(); }
		bool				IsMoving()				{ return m_isMoving || IsInEndingAnimation(); }
		bool				IsInEndingAnimation()	{return m_endingState != EEndingState::EEnding_Idle;}

	private:
		void Reset(DirectX::XMFLOAT2 initialPosition);
		void WinAnimationUpdate(float dTime);


		//Members
	private:
		bool					m_isMoving;
		DirectX::XMFLOAT3		m_targetPosition;
		MeshObject*				m_meshObject;
		float					m_currentRotation;

		//fields and enum for win animation
		enum EEndingState
		{
			EEnding_Idle = 0,
			EEnding_Lifting,
			EEnding_Traveling,
			EEnding_Landing
		};

		EEndingState			m_endingState;
		float					m_animationElapse = 0;
		DirectX::XMFLOAT3		m_animationStartPos;
		DirectX::XMFLOAT3		m_animationEndPos;
	};
}

