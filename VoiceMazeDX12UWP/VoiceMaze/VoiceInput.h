#pragma once


namespace VoiceMazeDX12UWP
{
	private class VoiceInput
	{
		//Methods
	public:
		VoiceInput();
		~VoiceInput();
		
		void SetInputConstraints(Windows::Foundation::Collections::IIterable<Platform::String^> ^inConstraints);

		void Start();
		void Stop();
		bool IsActive() { return m_isActive; }

		Platform::String^ GetLastReading(bool clearValue);

	private:
		void StartRecognitionInstance();


		//Members
	private:
		Platform::String^ m_lastReadingValue;
		bool	m_isActive;
		
		Windows::Foundation::Collections::IVector<Platform::String^> ^m_recognitionConstraints;
		Windows::Media::SpeechRecognition::SpeechRecognizer ^m_speechRecognizer;
	};
}

