#pragma once

#include "Common\DirectXHelper.h"

namespace VoiceMazeDX12UWP
{
	template <typename T>
	private class BufferDescriptor
	{
		//Methods
	public:
		BufferDescriptor<T>()
		{
		}

		~BufferDescriptor<T>() 
		{
			m_constantBuffer->Unmap(0, nullptr);
			m_mappedConstantBuffer = nullptr;
		}

		void CreateConstantBuffer(ID3D12Device *d3dDevice)
		{
			ZeroMemory(&m_constantBufferData, sizeof(m_constantBufferData));

			// Create a descriptor heap for the constant buffers.
			{
				D3D12_DESCRIPTOR_HEAP_DESC heapDesc = {};
				heapDesc.NumDescriptors = DX::c_frameCount;
				heapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
				// This flag indicates that this descriptor heap can be bound to the pipeline and that descriptors contained in it can be referenced by a root table.
				heapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;
				DX::ThrowIfFailed(d3dDevice->CreateDescriptorHeap(&heapDesc, IID_PPV_ARGS(&m_cbvHeap)));

				NAME_D3D12_OBJECT(m_cbvHeap);
			}

			CD3DX12_HEAP_PROPERTIES uploadHeapProperties(D3D12_HEAP_TYPE_UPLOAD);
			CD3DX12_RESOURCE_DESC constantBufferDesc = CD3DX12_RESOURCE_DESC::Buffer(DX::c_frameCount * c_alignedConstantBufferSize);
			DX::ThrowIfFailed(d3dDevice->CreateCommittedResource(
				&uploadHeapProperties,
				D3D12_HEAP_FLAG_NONE,
				&constantBufferDesc,
				D3D12_RESOURCE_STATE_GENERIC_READ,
				nullptr,
				IID_PPV_ARGS(&m_constantBuffer)));

			NAME_D3D12_OBJECT(m_constantBuffer);

			// Create constant buffer views to access the upload buffer.
			D3D12_GPU_VIRTUAL_ADDRESS cbvGpuAddress = m_constantBuffer->GetGPUVirtualAddress();
			CD3DX12_CPU_DESCRIPTOR_HANDLE cbvCpuHandle(m_cbvHeap->GetCPUDescriptorHandleForHeapStart());
			m_cbvDescriptorSize = d3dDevice->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV);

			for (int n = 0; n < DX::c_frameCount; n++)
			{
				D3D12_CONSTANT_BUFFER_VIEW_DESC desc;
				desc.BufferLocation = cbvGpuAddress;
				desc.SizeInBytes = c_alignedConstantBufferSize;
				d3dDevice->CreateConstantBufferView(&desc, cbvCpuHandle);

				cbvGpuAddress += desc.SizeInBytes;
				cbvCpuHandle.Offset(m_cbvDescriptorSize);

			}

			// Map the constant buffers.
			CD3DX12_RANGE readRange(0, 0);		// We do not intend to read from this resource on the CPU.
			DX::ThrowIfFailed(m_constantBuffer->Map(0, &readRange, reinterpret_cast<void**>(&m_mappedConstantBuffer)));
			ZeroMemory(m_mappedConstantBuffer, DX::c_frameCount * c_alignedConstantBufferSize);
			// We don't unmap this until the app closes. Keeping things mapped for the lifetime of the resource is okay.
		}

		void UpdateBuffer(T newData, UINT frameIndex, UINT rootParameterIndex, const Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList> &commandList)
		{
			ID3D12DescriptorHeap* ppHeaps[] = { m_cbvHeap.Get() };
			commandList->SetDescriptorHeaps(_countof(ppHeaps), ppHeaps);

			//// Bind the current frame's constant buffer to the pipeline.
			CD3DX12_GPU_DESCRIPTOR_HANDLE gpuHandle(m_cbvHeap->GetGPUDescriptorHandleForHeapStart(), frameIndex, m_cbvDescriptorSize);
			commandList->SetGraphicsRootDescriptorTable(rootParameterIndex, gpuHandle);

			m_constantBufferData = newData;

			UINT8* destination = m_mappedConstantBuffer + (frameIndex * c_alignedConstantBufferSize);
			memcpy(destination, &m_constantBufferData, sizeof(m_constantBufferData));
		}


		//Members
	private:
		// Constant buffers must be 256-byte aligned.
		static const UINT c_alignedConstantBufferSize = (sizeof(T) + 255) & ~255;

		Microsoft::WRL::ComPtr<ID3D12DescriptorHeap>		m_cbvHeap;
		Microsoft::WRL::ComPtr<ID3D12Resource>				m_constantBuffer;
		T													m_constantBufferData;

		UINT8*												m_mappedConstantBuffer;
		UINT												m_cbvDescriptorSize;
	};

}