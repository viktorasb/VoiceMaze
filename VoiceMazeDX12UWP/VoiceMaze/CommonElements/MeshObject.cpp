#include "pch.h"
#include "MeshObject.h"
#include <d3d12.h>

#include "Texture.h"

using namespace VoiceMazeDX12UWP;
using namespace DirectX;

MeshObject::MeshObject(std::vector<VertexPositionTextureNormal> inVertexBuffer, std::vector<unsigned short> inIndexBuffer, Texture* inTexture, ID3D12Device *d3dDevice, const Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList> &commandList)
	: RenderableObject(inTexture)
{
	CreateVertexBuffer(inVertexBuffer, d3dDevice, commandList);
	CreateIndexBuffer(inIndexBuffer, d3dDevice, commandList);
	CreateConstantBuffer(d3dDevice);
}
