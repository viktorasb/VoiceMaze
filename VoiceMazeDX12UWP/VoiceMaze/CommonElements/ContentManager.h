#pragma once

#include <wincodec.h>
#include "MeshObject.h"
#include "Texture.h"
#include "Loaders\MeshLoader.h"


namespace VoiceMazeDX12UWP
{
	class ContentManager
	{
		//Methods
	public:
		ContentManager(ID3D12Device *inD3dDevice, LPCWSTR inBaseDir);
		~ContentManager();

#pragma region Loading templates
		template <typename T> T* Load(LPCWSTR fileName, ID3D12GraphicsCommandList* commandList) { throw std::invalid_argument("This type cannot be loaded"); }
		template <> Texture* Load<Texture>(LPCWSTR fileName, ID3D12GraphicsCommandList* commandList)
		{
			for (unsigned int i = 0; i < m_names.size(); i++)
			{
				if (m_names[i] == fileName)
					return new Texture(m_descriptors[i]);
			}

			if (!PerformFullTextureLoad(fileName, commandList))
			{
				throw std::invalid_argument("could not load the item");
			}

			return new Texture(m_descriptors[m_descriptors.size() - 1]);
		}
		
		template <> MeshObject* Load<MeshObject>(LPCWSTR fileName, ID3D12GraphicsCommandList* commandList)
		{
			auto modelPath = (std::wstring(m_baseDir) + L"\\" + fileName + L".obj");
			auto texturePath = (std::wstring(fileName) + L".png");

			auto texture = Load<Texture>(texturePath.c_str(), commandList);


			std::vector<unsigned short> indexBuffer;
			std::vector<VertexPositionTextureNormal> vertexBuffer;
			//std::vector
			MeshLoader::LoadMesh(&vertexBuffer, &indexBuffer, modelPath.c_str(), m_d3dDevice, commandList);

			auto mesh = new MeshObject(vertexBuffer, indexBuffer, texture, m_d3dDevice, commandList);
			return mesh;
		}
#pragma endregion

	private:
		bool PerformFullTextureLoad(LPCWSTR fileName, ID3D12GraphicsCommandList* commandList);

		DXGI_FORMAT GetDXGIFormatFromWICFormat(WICPixelFormatGUID& wicFormatGUID);
		WICPixelFormatGUID GetConvertToWICFormat(WICPixelFormatGUID& wicFormatGUID);
		int GetDXGIFormatBitsPerPixel(DXGI_FORMAT& dxgiFormat);
		int LoadImageDataFromFile(BYTE** imageData, D3D12_RESOURCE_DESC& resourceDescription, LPCWSTR filename, int &bytesPerRow);


		//Members
	private:
		ID3D12Device*										m_d3dDevice;
		LPCWSTR												m_baseDir;

		std::vector<LPCWSTR>				m_names;
		std::vector<ID3D12DescriptorHeap*>	m_descriptors;
	};

}