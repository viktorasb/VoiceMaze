#pragma once

#include "..\BaseClasses\RenderableObject.h"
#include "..\Content\ShaderStructures.h"

namespace VoiceMazeDX12UWP
{
	class MeshObject : public RenderableObject
	{
		//Methods
	public:
		MeshObject(std::vector<VertexPositionTextureNormal> inVertexBuffer, std::vector<unsigned short> inIndexBuffer, Texture* inTexture, ID3D12Device *d3dDevice, const Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList> &commandList);

	private:


		//Members
	};

}