#pragma once

#include <wincodec.h>

namespace VoiceMazeDX12UWP
{
	private class ImageLoader sealed
	{
		//Methods
	public:
		static ID3D12DescriptorHeap* LoadTextureData(LPCWSTR fileName, ID3D12Device* d3dDevice, const Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList> &commandList);

	private:
		static DXGI_FORMAT GetDXGIFormatFromWICFormat(WICPixelFormatGUID& wicFormatGUID);
		static WICPixelFormatGUID GetConvertToWICFormat(WICPixelFormatGUID& wicFormatGUID);
		static int GetDXGIFormatBitsPerPixel(DXGI_FORMAT& dxgiFormat);
		static int LoadImageDataFromFile(BYTE** imageData, D3D12_RESOURCE_DESC& resourceDescription, LPCWSTR filename, int &bytesPerRow);


		//Members
	};

}