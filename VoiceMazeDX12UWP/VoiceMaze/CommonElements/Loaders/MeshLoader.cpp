#include "pch.h"

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <iterator>
#include <wincodec.h>

#include "../Content/ShaderStructures.h"
#include "MeshLoader.h"


using namespace DirectX;
using namespace VoiceMazeDX12UWP;

void MeshLoader::LoadMesh(std::vector<VertexPositionTextureNormal> *vertexBufferOut, std::vector<unsigned short> *indexBufferOut, LPCWSTR meshPath, ID3D12Device *d3dDevice, const Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList> &commandList, bool invertIndexWinding)
{
	std::ifstream modelFile = std::ifstream(meshPath);
	std::string line;

	std::vector<XMFLOAT3> vertexBuffer;
	std::vector<XMFLOAT3> faceNormals;
	std::vector<XMFLOAT3> texCoords;

	std::vector<XMFLOAT4> faceDefinitions;

	std::vector<unsigned short> finalIndexBuffer;
	std::vector<VertexPositionTextureNormal> finalVertexBuffer;
	int index = 0;
	while (std::getline(modelFile, line))
	{
		//std::istringstream iss(line);


		//f-index  v-vertex  vt-texture  vn-normal
		if (line.length() <= 2)
			continue;

		if (line[0] == 'f')
		{
			std::vector<std::string> vstrings = ExplodeStr(line);
			for (unsigned int i = 1; i < vstrings.size(); i++)
			{
				std::vector<std::string> faceDefStr = SplitByDelimiter(vstrings[i], '/');

				int index_v = std::atoi(faceDefStr[0].c_str());
				XMFLOAT3 vertex = vertexBuffer.at(index_v - 1);
				XMFLOAT3 texCoord(0, 0, 0);
				XMFLOAT3 normal(0, 0, 0);
				if (faceDefStr.size() > 1)
				{
					if (faceDefStr.size() > 2)
					{
						if (faceDefStr[1].length() >0)
						{
							int index_tc = std::atoi(faceDefStr[1].c_str());
							texCoord = texCoords[index_tc - 1];
						}
						int index_n = std::atoi(faceDefStr[2].c_str());
						normal = faceNormals.at(index_n - 1);
					}
					else
					{
						int index_n = std::atoi(faceDefStr[1].c_str());
						normal = faceNormals.at(index_n - 1);
					}
				}

				finalIndexBuffer.push_back(index);

				auto element = VertexPositionTextureNormal{ vertex, normal, XMFLOAT2(texCoord.x, texCoord.y) };
				finalVertexBuffer.push_back(element);
				index++;
			}
		}
		else if (line[0] == 'v')
		{
			if (line[1] == ' ')
			{
				std::vector<std::string> vstrings = ExplodeStr(line);
				float valX = (float)std::atof(vstrings[1].c_str()),
					valY = (float)std::atof(vstrings[2].c_str()),
					valZ = (float)std::atof(vstrings[3].c_str());
				vertexBuffer.push_back(XMFLOAT3(valX, valY, valZ));
			}
			else if (line[1] == 't')
			{
				std::vector<std::string> vstrings = ExplodeStr(line);
				float valX = (float)std::atof(vstrings[1].c_str()),
					valY = (float)std::atof(vstrings[2].c_str()),
					valZ = (float)std::atof(vstrings[3].c_str());
				texCoords.push_back(XMFLOAT3(valX, valY, valZ));
			}
			else if (line[1] == 'n')
			{
				std::vector<std::string> vstrings = ExplodeStr(line);
				float valX = (float)std::atof(vstrings[1].c_str()),
					valY = (float)std::atof(vstrings[2].c_str()),
					valZ = (float)std::atof(vstrings[3].c_str());
				faceNormals.push_back(XMFLOAT3(valX, valY, valZ));
			}
		}
	}

	if (invertIndexWinding)
		finalIndexBuffer = InvertWinding(finalIndexBuffer);

	for (unsigned int i = 0; i < finalVertexBuffer.size(); i++)
		vertexBufferOut->push_back(finalVertexBuffer[i]);
	for (unsigned int i = 0; i < finalIndexBuffer.size(); i++)
		indexBufferOut->push_back(finalIndexBuffer[i]);
}

std::vector<unsigned short> MeshLoader::InvertWinding(std::vector<unsigned short> originalIndices)
{
	std::vector<unsigned short> inverted;

	for (unsigned int i = 0; i < originalIndices.size() / 3; i++)
	{
		inverted.push_back(originalIndices[i * 3 + 0]);
		inverted.push_back(originalIndices[i * 3 + 2]);
		inverted.push_back(originalIndices[i * 3 + 1]);
	}

	return inverted;
}

std::vector<std::string> MeshLoader::SplitByDelimiter(const std::string &s, char delim)
{
	std::vector<std::string> elems;
	std::stringstream ss(s);
	std::string item;
	while (std::getline(ss, item, delim)) {
		elems.push_back(item);
	}
	return elems;
}

std::vector<std::string> MeshLoader::ExplodeStr(std::string aStr)
{
	std::stringstream ss(aStr);
	std::istream_iterator<std::string> begin(ss);
	std::istream_iterator<std::string> end;
	std::vector<std::string> vstrings(begin, end);
	return vstrings;
}