#pragma once

#include "..\..\..\Content\ShaderStructures.h"

namespace VoiceMazeDX12UWP
{
	//.OBJ models only
	private class MeshLoader sealed
	{
	public:
		//Methods
		static void LoadMesh(std::vector<VertexPositionTextureNormal> *vertexBuffer, std::vector<unsigned short> *indexBuffer, LPCWSTR meshPath, ID3D12Device *d3dDevice, const Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList> &commandList, bool invertIndexWinding = false);

	private:
		static std::vector<unsigned short> InvertWinding(std::vector<unsigned short> originalIndices);
		static std::vector<std::string> SplitByDelimiter(const std::string &s, char delim);
		static std::vector<std::string> ExplodeStr(std::string aStr);


		//Members
	};
}