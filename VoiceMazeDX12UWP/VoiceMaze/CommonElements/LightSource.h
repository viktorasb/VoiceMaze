#pragma once

namespace VoiceMazeDX12UWP
{
	private class LightSource sealed
	{
		//Methods
	public:
		LightSource();
		LightSource(DirectX::XMFLOAT4 inColor, DirectX::XMFLOAT4 inDirection);

		void SetColor(DirectX::XMFLOAT4 inColor)					{ m_lightColor = inColor; }
		void SetLightDirection(DirectX::XMFLOAT4 inLightDirection)	{ m_lightDirection = inLightDirection; }
		void SetLightPosition(DirectX::XMFLOAT3 inLightPosition)	{ m_lightPosition = inLightPosition; }

		DirectX::XMFLOAT4 GetColor()								{ return m_lightColor; }
		DirectX::XMFLOAT4 GetDirection()							{ return m_lightDirection; }
		DirectX::XMFLOAT3 GetPosition()								{ return m_lightPosition; }
		

		//Members
	private:
		DirectX::XMFLOAT4				m_lightDirection;
		DirectX::XMFLOAT4				m_lightColor;
		DirectX::XMFLOAT3				m_lightPosition;
	};

}