#pragma once

#include <Windows.h>

namespace VoiceMazeDX12UWP
{
	private class GameInput sealed
	{
		//Methods
	public:
		GameInput();

		bool IsKeyPressed(Windows::System::VirtualKey key);


		//Members
	private:
		Windows::UI::Core::CoreWindow^		m_coreWindow;
	};
}