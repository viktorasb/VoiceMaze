#include "pch.h"
#include "ContentManager.h"
#include "Loaders\ImageLoader.h"

using namespace VoiceMazeDX12UWP;

ContentManager::ContentManager(ID3D12Device *inD3dDevice, LPCWSTR inBaseDir = L"")
{
	m_baseDir = inBaseDir;
	m_d3dDevice = inD3dDevice;

	m_names = {};
	m_descriptors = {};
}

ContentManager::~ContentManager()
{
	m_names.clear();
	for (unsigned int i = 0; i < m_descriptors.size(); i++)
	{
		auto descriptor = m_descriptors[i];
		// we are done with image data now that we've uploaded it to the gpu, so free it up
		descriptor->Release();
	}
	m_descriptors.clear();
}


bool ContentManager::PerformFullTextureLoad(LPCWSTR fileName, ID3D12GraphicsCommandList* commandList)
{
	auto finalPath = (std::wstring(m_baseDir) + L"\\" + fileName);

	auto textureDescriptor = ImageLoader::LoadTextureData(finalPath.c_str(), m_d3dDevice, commandList);

	if (textureDescriptor == nullptr)
		return false;

	m_descriptors.push_back(textureDescriptor);
	m_names.push_back(fileName);

	return true;
}