#pragma once


namespace VoiceMazeDX12UWP
{
	//Texture container
	class Texture
	{
		// Methods
	public:
		Texture(ID3D12DescriptorHeap *inTexture)
		{
			m_texture = inTexture;
		}

		Texture(ID3D12Device* d3dDevice, uint64 width, uint64 height, DXGI_FORMAT format);

		~Texture();

		ID3D12DescriptorHeap* GetTextureDescriptor()
		{
			return m_texture;
		}

	protected:
		Texture() {};


		// Members
	protected:
		ID3D12DescriptorHeap *m_texture;

	private:
		bool m_selfInstantiated;
	};
}