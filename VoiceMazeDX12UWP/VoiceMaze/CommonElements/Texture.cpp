#include "pch.h"
#include "Texture.h"


using namespace VoiceMazeDX12UWP;

Texture::~Texture()
{
	if (m_selfInstantiated)
	{
		delete m_texture;
		m_texture = nullptr;
	}
}

Texture::Texture(ID3D12Device* d3dDevice, uint64 width, uint64 height, DXGI_FORMAT format)
{
	throw std::invalid_argument("Function was never tested. If you feel lucky, comment out the exception and use at your own risk");

	HRESULT hr;
	ID3D12Resource* textureBuffer;
	ID3D12Resource* textureBufferUploadHeap;
	ID3D12DescriptorHeap* descriptorHeap;

	// Load the image from file
	D3D12_RESOURCE_DESC textureDesc;
	textureDesc.Format = format;

	// create a default heap where the upload heap will copy its contents into (contents being the texture)
	hr = d3dDevice->CreateCommittedResource(
		&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_DEFAULT), // a default heap
		D3D12_HEAP_FLAG_NONE, // no flags
		&textureDesc, // the description of our texture
		D3D12_RESOURCE_STATE_COPY_DEST, // We will copy the texture from the upload heap to here, so we start it out in a copy dest state
		nullptr, // used for render targets and depth/stencil buffers
		IID_PPV_ARGS(&textureBuffer));
	if (FAILED(hr))
	{
		throw new std::invalid_argument("Could not create the default heap for texture");
	}
	textureBuffer->SetName(L"Texture Buffer Resource Heap");


	// now we create an upload heap to upload our texture to the GPU
	hr = d3dDevice->CreateCommittedResource(
		&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD), // upload heap
		D3D12_HEAP_FLAG_NONE, // no flags
		&CD3DX12_RESOURCE_DESC::Buffer(width * height), // resource description for a buffer (storing the image data in this heap just to copy to the default heap)
		D3D12_RESOURCE_STATE_GENERIC_READ, // We will copy the contents from this heap to the default heap above
		nullptr,
		IID_PPV_ARGS(&textureBufferUploadHeap));
	if (FAILED(hr))
	{
		throw new std::invalid_argument("Could not upload texture to GPU");
	}
	textureBufferUploadHeap->SetName(L"Texture Buffer Upload Resource Heap");


	// create the descriptor heap that will store our srv
	D3D12_DESCRIPTOR_HEAP_DESC heapDesc = {};
	heapDesc.NumDescriptors = 1;
	heapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;
	heapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
	hr = d3dDevice->CreateDescriptorHeap(&heapDesc, IID_PPV_ARGS(&descriptorHeap));
	if (FAILED(hr))
	{
		throw new std::invalid_argument("Could not create heap descriptor");
	}

	// now we create a shader resource view (descriptor that points to the texture and describes it)
	D3D12_SHADER_RESOURCE_VIEW_DESC srvDesc = {};
	srvDesc.Shader4ComponentMapping = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;
	srvDesc.Format = textureDesc.Format;
	srvDesc.ViewDimension = D3D12_SRV_DIMENSION_TEXTURE2D;
	srvDesc.Texture2D.MipLevels = 1;
	d3dDevice->CreateShaderResourceView(textureBuffer, &srvDesc, descriptorHeap->GetCPUDescriptorHandleForHeapStart());

	m_texture = descriptorHeap;
	m_selfInstantiated = true;
}