#include "pch.h"
#include "GameInput.h"

using namespace DirectX;
using namespace VoiceMazeDX12UWP;

using namespace Windows::UI::Core;

GameInput::GameInput()
{
	auto coreWindow = Windows::ApplicationModel::Core::CoreApplication::GetCurrentView()->CoreWindow;
	m_coreWindow = coreWindow;
}


bool GameInput::IsKeyPressed(Windows::System::VirtualKey key)
{
	
	auto result = m_coreWindow->GetKeyState(key);
	
	return (int)result & (int)Windows::UI::Core::CoreVirtualKeyStates::Down;
}