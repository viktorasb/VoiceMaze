#include "pch.h"
#include "LightSource.h"
#include <d3d12.h>

using namespace DirectX;
using namespace VoiceMazeDX12UWP;

LightSource::LightSource()
{
	m_lightColor = XMFLOAT4(0, 0, 0, 0);
	m_lightDirection = XMFLOAT4(0, 0, 0, 0);
}

LightSource::LightSource(DirectX::XMFLOAT4 inColor, DirectX::XMFLOAT4 inDirection)
{
	m_lightColor = inColor;
	m_lightDirection = inDirection;
}