#pragma once

#include "../Common/DeviceResources.h"

namespace VoiceMazeDX12UWP
{

	private class Camera sealed
	{
		//Methods
	public:
		Camera(std::shared_ptr<DX::DeviceResources>);
		void Initialize(Windows::Foundation::Size screenSize, D3D12_VIEWPORT viewport);

		void SetEyePosition(DirectX::XMFLOAT4 inEyePosition) { m_eyePosition = inEyePosition; }
		void SetLookAtPoint(DirectX::XMFLOAT4 inLookAtPosition) { m_lookAtPosition = inLookAtPosition; }
		void SetUpVector(DirectX::XMFLOAT4 inUp) { m_up = inUp; }

		void SetFoV(float inFoVAngle) { m_foVAngle = inFoVAngle; }
		void SetAspectRatio(float inAspectRatio) { m_aspectRatio = inAspectRatio; }

		void SetScissorRectangle(D3D12_RECT inScissorRectangle) { m_scissorRect = inScissorRectangle; }
		void SetNearPlane(float inNearPlane) { m_nearPlane = inNearPlane; }
		void SetFarPlane(float inFarPlane) { m_farPlane = inFarPlane; }

		DirectX::XMFLOAT4 GetViewVector();
		DirectX::XMFLOAT4X4 CalculateProjection();
		DirectX::XMFLOAT4X4 CalculateView();
		DirectX::XMMATRIX CalculateProjectionMat();
		DirectX::XMMATRIX CalculateViewMat();
		D3D12_RECT GetScissorRectangle() { return m_scissorRect; }


		//Members
	private:

		std::shared_ptr<DX::DeviceResources>	m_deviceResources;
		DirectX::XMFLOAT4						m_eyePosition;
		DirectX::XMFLOAT4						m_lookAtPosition;
		DirectX::XMFLOAT4						m_up;

		float								m_foVAngle;
		float								m_aspectRatio;

		D3D12_RECT							m_scissorRect;
		float								m_nearPlane;
		float								m_farPlane;
	};

}