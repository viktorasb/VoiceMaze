#include "pch.h"
#include "Camera.h"
#include <d3d12.h>

using namespace DirectX;
using namespace Windows::Foundation;
using namespace VoiceMazeDX12UWP;

Camera::Camera(std::shared_ptr<DX::DeviceResources> inDeviceResource)
{
	m_deviceResources = inDeviceResource;
}

void Camera::Initialize(Size screenSize, D3D12_VIEWPORT viewport)
{
	SetEyePosition(XMFLOAT4{ 0.0f, 0.7f, 1.5f, 0.0f });
	SetLookAtPoint(XMFLOAT4{ 0.0f, 0.0f, 0.0f, 0.0f });
	SetUpVector(XMFLOAT4{ 0.0f, 1.0f, 0.0f, 0.0f });

	SetFoV(70.0f * XM_PI / 180.0f);
	SetAspectRatio(screenSize.Width / screenSize.Height);

	SetScissorRectangle({ 0, 0, static_cast<LONG>(viewport.Width), static_cast<LONG>(viewport.Height) });

	SetNearPlane(0.01f);
	SetFarPlane(1000.0f);
}

XMMATRIX Camera::CalculateProjectionMat()
{
	XMMATRIX perspectiveMatrix = XMMatrixPerspectiveFovLH(
		m_foVAngle,
		m_aspectRatio,
		m_nearPlane,
		m_farPlane
	);

	XMMATRIX orientationMatrix = XMLoadFloat4x4(&m_deviceResources->GetOrientationTransform3D());

	return perspectiveMatrix * orientationMatrix;
}

XMMATRIX Camera::CalculateViewMat()
{
	return XMMatrixLookAtLH(XMLoadFloat4(&m_eyePosition), XMLoadFloat4(&m_lookAtPosition), XMLoadFloat4(&m_up));
}

XMFLOAT4X4 Camera::CalculateView()
{
	XMFLOAT4X4 view;

	auto viewM = CalculateViewMat();
	XMStoreFloat4x4(&view, viewM);

	return view;
}

XMFLOAT4X4 Camera::CalculateProjection()
{
	XMFLOAT4X4 projection;

	auto projectionM = CalculateProjectionMat();
	XMStoreFloat4x4(&projection, projectionM);

	return projection;
}

DirectX::XMFLOAT4 Camera::GetViewVector()
{
	auto difference = XMLoadFloat4(&m_lookAtPosition) - XMLoadFloat4(&m_eyePosition);
	auto transformedVector = XMVector3Transform(difference, XMMatrixRotationY(0));
	XMFLOAT4 transformedVectorFloat4;

	XMStoreFloat4(&transformedVectorFloat4, transformedVector);

	return transformedVectorFloat4;
}