#pragma once

using namespace DirectX;

namespace VoiceMazeDX12UWP
{
	inline XMFLOAT3 operator-(const XMFLOAT3 f1, const XMFLOAT3 f2)
	{
		return XMFLOAT3(f1.x - f2.x, f1.y - f2.y, f1.z - f2.z);
	}

	inline XMFLOAT3 operator+(const XMFLOAT3 f1, const XMFLOAT3 f2)
	{
		return XMFLOAT3(f1.x + f2.x, f1.y + f2.y, f1.z + f2.z);
	}

	inline XMFLOAT3 operator*(const XMFLOAT3 f1, const float f2)
	{
		return XMFLOAT3(f1.x *f2, f1.y *f2, f1.z *f2);
	}

	inline float LengthSquared(const XMFLOAT3 f1)
	{
		return f1.x * f1.x + f1.y * f1.y + f1.z * f1.z;
	}

	inline float Length(const XMFLOAT3 f1)
	{
		return sqrtf(LengthSquared(f1));
	}

	inline XMFLOAT3 Normalized(const XMFLOAT3 f1)
	{
		auto length = Length(f1);
		return XMFLOAT3(f1.x / length, f1.y / length, f1.z / length);
	}
}