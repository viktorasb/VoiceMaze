#pragma once

#include "CommonElements\Texture.h"
#include "BaseClasses\RenderableObject.h"

namespace VoiceMazeDX12UWP
{
	private class MazeElement : public RenderableObject
	{
		//Methods
	public:
		MazeElement(ID3D12Device *d3dDevice, const Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList> &commandList, Texture *inTexture);


		//Members
	};
}

