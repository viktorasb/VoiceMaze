#include "pch.h"
#include "MazeElement.h"
#include "Content\ShaderStructures.h"

using namespace VoiceMazeDX12UWP;
using namespace DirectX;

MazeElement::MazeElement(ID3D12Device *d3dDevice, const Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList> &commandList, Texture *inTexture)
	:RenderableObject(inTexture)
{
	// Cube vertices. Each vertex has a position and a color.
	std::vector<VertexPositionTextureNormal> cubeVertices =
	{
		//top
		{ XMFLOAT3(-0.5f,	0.5f,	-0.5f),		XMFLOAT3(0, 1, 0),	XMFLOAT2(1.0f, 1.0f) },
		{ XMFLOAT3(-0.5f,	0.5f,	 0.5f),		XMFLOAT3(0, 1, 0),	XMFLOAT2(1.0f, 0.0f) },
		{ XMFLOAT3( 0.5f ,	0.5f,	 0.5f),		XMFLOAT3(0, 1, 0),	XMFLOAT2(0.0f, 0.0f) },
		{ XMFLOAT3( 0.5f ,	0.5f,	-0.5f),		XMFLOAT3(0, 1, 0),	XMFLOAT2(0.0f, 1.0f) },

		//left
		{ XMFLOAT3(-0.5f,	-0.5f,	 0.5f),		XMFLOAT3(-1, 0, 0),	XMFLOAT2(1.0f, 1.0f) },
		{ XMFLOAT3(-0.5f,	 0.5f,	 0.5f),		XMFLOAT3(-1, 0, 0),	XMFLOAT2(1.0f, 0.0f) },
		{ XMFLOAT3(-0.5f,	 0.5f,	-0.5f),		XMFLOAT3(-1, 0, 0),	XMFLOAT2(0.0f, 0.0f) },
		{ XMFLOAT3(-0.5f,	-0.5f,	-0.5f),		XMFLOAT3(-1, 0, 0),	XMFLOAT2(0.0f, 1.0f) },

		//back
		{ XMFLOAT3( 0.5f,	-0.5f,	 0.5f),		XMFLOAT3(0, 0, 1),	XMFLOAT2(1.0f, 1.0f) },
		{ XMFLOAT3( 0.5f,	 0.5f,	 0.5f),		XMFLOAT3(0, 0, 1),	XMFLOAT2(1.0f, 0.0f) },
		{ XMFLOAT3(-0.5f,	 0.5f,	 0.5f),		XMFLOAT3(0, 0, 1),	XMFLOAT2(0.0f, 0.0f) },
		{ XMFLOAT3(-0.5f,	-0.5f,	 0.5f),		XMFLOAT3(0, 0, 1),	XMFLOAT2(0.0f, 1.0f) },
		
		//right
		{ XMFLOAT3( 0.5f,	-0.5f,	-0.5f),		XMFLOAT3(1, 0, 0),	XMFLOAT2(1.0f, 1.0f) },
		{ XMFLOAT3( 0.5f,	 0.5f,	-0.5f),		XMFLOAT3(1, 0, 0),	XMFLOAT2(1.0f, 0.0f) },
		{ XMFLOAT3( 0.5f,	 0.5f,	 0.5f),		XMFLOAT3(1, 0, 0),	XMFLOAT2(0.0f, 0.0f) },
		{ XMFLOAT3( 0.5f,	-0.5f,	 0.5f),		XMFLOAT3(1, 0, 0),	XMFLOAT2(0.0f, 1.0f) },

		//front
		{ XMFLOAT3(-0.5f,	-0.5f,	-0.5f),		XMFLOAT3(0, 0, -1),	XMFLOAT2(1.0f, 1.0f) },
		{ XMFLOAT3(-0.5f,	 0.5f,	-0.5f),		XMFLOAT3(0, 0, -1),	XMFLOAT2(1.0f, 0.0f) },
		{ XMFLOAT3( 0.5f,	 0.5f,	-0.5f),		XMFLOAT3(0, 0, -1),	XMFLOAT2(0.0f, 0.0f) },
		{ XMFLOAT3( 0.5f,	-0.5f,	-0.5f),		XMFLOAT3(0, 0, -1),	XMFLOAT2(0.0f, 1.0f) },

		//bottom
		{ XMFLOAT3( 0.5f,	-0.5f,	-0.5f),		XMFLOAT3(0, -1, 0),	XMFLOAT2(1.0f, 1.0f) },
		{ XMFLOAT3( 0.5f,	-0.5f,	 0.5f),		XMFLOAT3(0, -1, 0),	XMFLOAT2(1.0f, 0.0f) },
		{ XMFLOAT3(-0.5f,	-0.5f,	 0.5f),		XMFLOAT3(0, -1, 0),	XMFLOAT2(0.0f, 0.0f) },
		{ XMFLOAT3(-0.5f,	-0.5f,	-0.5f),		XMFLOAT3(0, -1, 0),	XMFLOAT2(0.0f, 1.0f) },
	};
	

	std::vector<unsigned short> cubeIndices =
	{
		0, 1, 2,
		0, 2, 3,

		4, 5, 6,
		4, 6, 7,

		8, 9, 10,
		8, 10, 11,

		12, 13, 14,
		12, 14, 15,

		16, 17, 18,
		16, 18, 19,

		20, 21, 22,
		20, 22, 23,
	};

	CreateConstantBuffer(d3dDevice);
	CreateVertexBuffer(cubeVertices, d3dDevice, commandList);
	CreateIndexBuffer(cubeIndices, d3dDevice, commandList);
}

