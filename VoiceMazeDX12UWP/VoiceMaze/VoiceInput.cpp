#include "pch.h"
#include "VoiceInput.h"
#include <ppltasks.h>
#include <collection.h>

using namespace VoiceMazeDX12UWP;
using namespace Windows::Media::SpeechRecognition;
using namespace Windows::Foundation::Collections;
using namespace Platform;
using namespace Platform::Collections;
using namespace concurrency;

VoiceInput::VoiceInput()
{
	m_recognitionConstraints = ref new Vector<Platform::String^>();

	IVector<String^> ^directions = ref new Vector<Platform::String^>;//array of voice commands to listen for

	directions->Append("up");
	directions->Append("down");
	directions->Append("left");
	directions->Append("right");

	SetInputConstraints(directions);
}

VoiceInput::~VoiceInput()
{

}

void VoiceInput::SetInputConstraints(IIterable<String^> ^inConstraints)
{
	m_recognitionConstraints->Clear();


	for each (String^ var in inConstraints)
		m_recognitionConstraints->Append(var);
}

Platform::String^ VoiceInput::GetLastReading(bool clearValue)
{
	auto readingValue = m_lastReadingValue;
	if (clearValue)
		m_lastReadingValue = L"";

	return readingValue;
}

void VoiceInput::Start()
{
	m_isActive = true;
	m_lastReadingValue = L"";

	m_speechRecognizer = ref new SpeechRecognizer();
	auto listConstraint = ref new SpeechRecognitionListConstraint(m_recognitionConstraints, L"TextTagIdWhatever");

	m_speechRecognizer->Constraints->Append(listConstraint);

	auto constraintsCompilationTask = create_task(m_speechRecognizer->CompileConstraintsAsync());

	constraintsCompilationTask.
		then( [this](SpeechRecognitionCompilationResult ^compilationResult)
		{
			StartRecognitionInstance();
		}
	);
}


void VoiceInput::StartRecognitionInstance()
{
	auto recognitionTask = create_task(m_speechRecognizer->RecognizeAsync());
	recognitionTask.then([this](SpeechRecognitionResult ^recognitionResult)
	{
		auto newValue = recognitionResult->Text;
		m_lastReadingValue = newValue;
		if (IsActive())
			StartRecognitionInstance();
	});
}

void VoiceInput::Stop()
{
	m_isActive = false;
}