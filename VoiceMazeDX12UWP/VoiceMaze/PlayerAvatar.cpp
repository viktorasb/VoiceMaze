#include "pch.h"
#include "PlayerAvatar.h"
#include "Helpers\XMFloatMath.h"

using namespace VoiceMazeDX12UWP;
using namespace DirectX;

PlayerAvatar::PlayerAvatar(MeshObject* inMeshObject)
{
	m_meshObject = inMeshObject;
	Reset(XMFLOAT2(0, 0));
}

PlayerAvatar::~PlayerAvatar()
{
	delete m_meshObject;
	m_meshObject = nullptr;
}

void PlayerAvatar::Reset(XMFLOAT2 initialPosition)
{
	m_meshObject->SetScale(KInitialScale);
	m_meshObject->SetPosition(XMFLOAT3(initialPosition.x, KInitialLift, initialPosition.y));
	m_endingState = EEndingState::EEnding_Idle;
}

void PlayerAvatar::Update(float dTime)
{
	if (IsInEndingAnimation())
		return WinAnimationUpdate(dTime);

	m_currentRotation += KIdleRotationSpeed * dTime;
	m_meshObject->SetRotation(XMFLOAT3(0, m_currentRotation, 0));

	if (!IsMoving())
		return;
	
	auto distance = m_targetPosition - GetCurrentPosition();
	if (LengthSquared(distance) < 0.005)
		m_isMoving = false;
	else
	{
		auto newPosition = GetCurrentPosition() + Normalized(distance) * KMovementSpeed * dTime;
		m_meshObject->SetPosition(newPosition);
	}
}

void PlayerAvatar::WinAnimationUpdate(float dTime)
{
	m_currentRotation += KWinRotationSpeed * dTime;
	m_meshObject->SetRotation(XMFLOAT3(0, m_currentRotation, 0));

	float ratio = m_animationElapse / KWinStepTime;

	switch (m_endingState)
	{
		case EEnding_Lifting:
		{
			auto startPos = m_animationStartPos;
			auto endPos = XMFLOAT3(m_animationStartPos.x, KWinLiftAmount, m_animationStartPos.x);
			auto pos = startPos + (endPos - startPos)*ratio;
			m_meshObject->SetPosition(pos);
			m_meshObject->SetScale(KInitialScale * (1 - ratio));
		}
		break;

		case EEnding_Traveling:
		{
			auto startPos = XMFLOAT3(m_animationStartPos.x, KWinLiftAmount, m_animationStartPos.x);
			auto endPos = XMFLOAT3(m_animationEndPos.x, KWinLiftAmount, m_animationEndPos.x);
			auto pos = startPos + (endPos - startPos)*ratio;
			m_meshObject->SetPosition(pos);
		}
		break;

		case EEnding_Landing:
		{
			auto startPos = XMFLOAT3(m_animationEndPos.x, KWinLiftAmount, m_animationEndPos.x);
			auto endPos = m_animationEndPos;
			auto pos = startPos + (endPos - startPos)*ratio;
			m_meshObject->SetPosition(pos);
			m_meshObject->SetScale(KInitialScale * (ratio));
		}
		break;
	}

	m_animationElapse += dTime;
	if (m_animationElapse >= KWinStepTime)
	{
		m_endingState = EEndingState(m_endingState+1);
		m_animationElapse = 0;
		if (m_endingState > EEndingState::EEnding_Landing)
			Reset(XMFLOAT2(m_animationEndPos.x, m_animationEndPos.z));
	}
}

void PlayerAvatar::PlayEndAnimation(XMFLOAT2 startPosition)
{
	m_animationEndPos = XMFLOAT3(startPosition.x, KInitialLift, startPosition.y);
	m_animationStartPos = GetCurrentPosition();
	m_endingState = EEndingState::EEnding_Lifting;
}

void PlayerAvatar::Render(const Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList> &commandList, UINT8 frameIndex, Camera* camera)
{
	m_meshObject->Render(commandList, frameIndex, camera);
}

void PlayerAvatar::MoveAvatar(XMFLOAT3 newPos, bool forceImmediate)
{
	m_targetPosition = newPos;
	if (forceImmediate)
	{
		m_meshObject->SetPosition(newPos);
		m_isMoving = false;
	}
	else m_isMoving = true;
}

XMFLOAT2 PlayerAvatar::GetCurrentPositionIndex()
{
	auto position = GetCurrentPosition();
	return XMFLOAT2(roundf(position.x), roundf(position.z));
}