#pragma once

#include "CommonElements\GameInput.h"
#include "VoiceInput.h"

namespace VoiceMazeDX12UWP
{
	enum EInput
	{
		EInput_NONE,
		EInput_UP,
		EInput_DOWN,
		EInput_LEFT,
		EInput_RIGHT,
	};

	private class MazeInput
	{
		//Methods
	public:
		MazeInput();
		~MazeInput();

		void UpdateInput();
		EInput GetLastInput() { return m_lastInput; };

	private:
	
		void ProcessKeyboardInput();
		bool ProcessVoiceInput();


		//Members
	private:
		GameInput				m_gameInput;
		VoiceInput				m_voiceInput;
		EInput					m_lastInput;
	};
}

