#pragma once

#include "MazeInput.h"
#include "PlayerAvatar.h"
#include "BaseClasses\RenderableObject.h"
#include "CommonElements\ContentManager.h"

namespace VoiceMazeDX12UWP
{
	private class Maze
	{
		//Methods
	public:
		Maze();
		~Maze();

		void Initialize(ID3D12Device *d3dDevice, const Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList> &commandList, ContentManager *contentManager);
		void Render(const Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList> &commandList, UINT8 frameIndex, Camera *camera);

		void ProcessInput(EInput input, PlayerAvatar *playerAvatar);
		bool IsExitFound(PlayerAvatar *playerAvatar);

		DirectX::XMFLOAT2 GetStartPosition() { return m_startPosition; }

	private:
		bool CheckIfEnclosed(const int *mazeData, int numRows, int numCols);


		//Members
	private:
		DirectX::XMFLOAT2								m_startPosition;

		std::vector<std::vector<int>>					m_mazeElementTypes;
		std::vector<std::vector<RenderableObject*>>		m_mazeElementInstances;
	};
}

