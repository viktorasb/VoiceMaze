﻿#pragma once

#include "Common\StepTimer.h"
#include "Common\DeviceResources.h"
#include "VoiceMaze\BaseClasses\GameBase.h"
#include "VoiceMaze\BaseClasses\Renderer3DBase.h"
#include "VoiceMaze\CommonElements\ContentManager.h"

// Renders Direct3D content on the screen.
namespace VoiceMazeDX12UWP
{
	class VoiceMazeDX12UWPMain : GameBase
	{
	public:
		VoiceMazeDX12UWPMain();
		void CreateRenderers(const std::shared_ptr<DX::DeviceResources>& deviceResources);
		void Update();
		bool Render();

		void OnWindowSizeChanged();
		void OnSuspending();
		void OnResuming();
		void OnDeviceRemoved();

	private:
		// TODO: Replace with your own content renderers.
		std::unique_ptr<Renderer3DBase> m_sceneRenderer;

		// Rendering loop timer.
		DX::StepTimer m_timer;
	};
}