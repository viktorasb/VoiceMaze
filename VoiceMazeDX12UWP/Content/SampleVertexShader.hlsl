cbuffer PerObjectConstantBuffer : register(b0)
{
	matrix Transformations;
    matrix WorldInverseTranspose;
    float4 PremultipliedViewVector;
};

cbuffer PerFrameConstantBuffer : register(b1)
{
    float4 DiffuseLightDirection;

    float4 DiffuseLightColor;
    float4 AmbientLightColor;
    float4 SpecularLightColor;

    float DiffuseLightIntensity;
    float AmbientLightIntensity;
    float Shininess;
    float SpecularLightIntensity;
};

// Per-vertex data used as input to the vertex shader.
struct VertexShaderInput
{
    float4 Position : POSITION;
    float4 Normal: NORMAL;
    float2 TexCoord : TEXCOORD;
};

struct VertexShaderOutput
{
    float4 Position : SV_POSITION;
    float4 Normal : NORMAL;
    float4 Color : COLOR;
    float4 PremultipliedViewVector : NORMAL1;
    float2 TexCoord : TEXCOORD;
};

VertexShaderOutput main(VertexShaderInput input)
{
    VertexShaderOutput output;
 
    output.Position = mul(input.Position, Transformations);
 
    float4 normal = normalize(mul(input.Normal, WorldInverseTranspose));
    float lightIntensity = dot(normal, DiffuseLightDirection);
    output.Color = saturate(DiffuseLightColor * DiffuseLightIntensity * lightIntensity);
 
    output.Normal = normal;
    output.TexCoord = input.TexCoord;
    output.PremultipliedViewVector = PremultipliedViewVector;

    return output;
}
