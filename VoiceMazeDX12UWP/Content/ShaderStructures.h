﻿#pragma once

namespace VoiceMazeDX12UWP
{
	// Constant buffer used to send MVP matrices to the vertex shader.
	struct PerObjectConstantBuffer
	{
		DirectX::XMFLOAT4X4 Transformations;
		DirectX::XMFLOAT4X4 WorldInverseTranspose;
		DirectX::XMFLOAT4 PremultipliedViewVector;
	};

	struct PerFrameConstantBuffer
	{
		DirectX::XMFLOAT4 DiffuseLightDirection;
		DirectX::XMFLOAT4 DiffuseLightColor;

		DirectX::XMFLOAT4 AmbientLightColor = DirectX::XMFLOAT4(1, 1, 1, 1);
		DirectX::XMFLOAT4 SpecularLightColor = DirectX::XMFLOAT4(1, 1, 1, 1);

		float DiffuseLightIntensity = 1.0f;
		float AmbientLightIntensity = 0.1f;
		float Shininess = 200.0f;
		float SpecularLightIntensity = 1.0f;
	};

	// Used to send per-vertex data to the vertex shader.
	struct VertexPositionTextureNormal
	{
		DirectX::XMFLOAT3 Pos;
		DirectX::XMFLOAT3 Normal;
		DirectX::XMFLOAT2 TexCoord;
	};
}