Texture2D Texture : register(t0);
SamplerState Sampler : register(s0);

cbuffer PerFrameConstantBuffer : register(b1)
{
    float4 DiffuseLightDirection;

    float4 DiffuseLightColor;
    float4 AmbientLightColor;
    float4 SpecularLightColor;

    float DiffuseLightIntensity;
    float AmbientLightIntensity;
    float Shininess;
    float SpecularLightIntensity;
};

struct PixelShaderInput
{
    float4 Position : SV_POSITION;
    float4 Normal : NORMAL;
    float4 Color : COLOR;
    float4 PremultipliedViewVector: NORMAL1;
    float2 TexCoord : TEXCOORD;
};


float4 main(PixelShaderInput input) : SV_TARGET
{
    float4 light = normalize(DiffuseLightDirection);
    float4 normal = normalize(input.Normal);
    float4 r = normalize(2 * dot(light, normal) * normal - light);
    float dotProduct = dot(r, input.PremultipliedViewVector);
 
    float4 specular = SpecularLightIntensity * SpecularLightColor * max(pow(dotProduct, Shininess), 0) * length(input.Color);
 
    float4 textureColor = Texture.Sample(Sampler, input.TexCoord);
 
    return saturate(textureColor * (input.Color + AmbientLightColor * AmbientLightIntensity) + specular);
}
